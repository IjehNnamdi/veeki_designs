/**
 * ClientsController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var unirest = require('unirest');

module.exports = {

  landingPage: function (req, res) {
    res.view('homepage');
  },

  dashboard: function (req, res) {
    res.view('client/dashboard');
  },


 reg: function (req, res) {
     res.view('client/signUp');
   },

  clientLog: function (req, res){
    res.view('client/login');
  },

  signIn: function (req, res) {
    // sign asynchronously
    var data = {email: req.body.email};
    const token = jwt.sign({data:data}, 'my_secret_key');

        clients.findOne(err, data)
        .then(function (data) {
          if(data === 1)
             res.redirect('client/dashboard');
            }).catch(function (err) {
              res.send(err+ 'Invalid client Id and/or Password');
           });

    jwt.verify(token, 'data', function(err) {
      if (err) {
          data = {
            name: 'TokenExpiredError',
            message: 'jwt expired',
            expiredAt: 1408621000
          }
          .exec( function () {
            res.ok(data)
          }).catch(function () {
            res.send(data.message)
          })
      }
    });


},


  //CLIENT REGISTRATION FORM WITH BYCRYPT-HASH
  signUp: function (req, res) {

           bcrypt.genSalt(10, function(err, salt) {
                           //  console.log(salt);
             bcrypt.hash(req.body.password, salt, function(err, hash) {
             // Store hash in your password DB.
               var data = {
                 "name" : req.body.fname,
                 "email": req.body.email,
                 "phone": req.body.phone,
                 "password": hash
               };
                       clients.create(data)
                               .then(function (err, data) {
                                      //res.json(data);
                                      res.redirect('client/login');
                                      })
                                        //check if client email already exist in the database
                                      .then( function (res) {
                                      if(data === clients.email)
                                       res.send("Client already exists on this Platform")
                                         //return redirect('client/signUp');
                                       })
                                       .catch( function (err) {
                                           res.ok(err+ "Unable to signUp Client")
                                     })
                               })
                             })
                           }

};

