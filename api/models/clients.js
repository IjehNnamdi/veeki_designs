/**
 * Clients.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

          attributes:
          {
                id: {
                      type: 'integer',
                      unique: true,
                      primaryKey: true,
                      columnName: 'the_primary_key'
                },

                name: {
                        type: 'string',
                        columnName: 'name'
                      },

                 email: {
                        type: 'string',
                        unique: true,
                        columnName: 'email'
                      } ,

                 phone: {
                      type:"integer",
                      unique: true,
                      columnName: "phone"

                  },

                   password: {
                         type: 'string',
                         columnName: 'seriously_encrypted_password'

                            }

                }


};

